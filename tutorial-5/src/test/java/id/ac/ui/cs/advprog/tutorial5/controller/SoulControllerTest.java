package id.ac.ui.cs.advprog.tutorial5.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = SoulController.class)
public class SoulControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SoulServiceImpl soulService;

    @Test
    public void testPageExistsAndContainsData() throws Exception {
        mockMvc.perform(get("/soul"))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateUser() throws Exception{
        Soul soul = new Soul();
        soul.setAge(22);
        soul.setGender("F");
        soul.setName("Taylor Swift");
        soul.setOccupation("Musician");
        soul.setId(1);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(soul);

        mockMvc.perform(post("/soul")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testFindUserByIdDoesNotExist() throws Exception{
        mockMvc.perform(get("/soul/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testFindUserByIdExist() throws Exception{
        Soul soul2 = new Soul();
        soul2.setAge(21);
        soul2.setGender("M");
        soul2.setName("Ashe Ubert");
        soul2.setOccupation("Commoner");
        soul2.setId(2);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(soul2);

        mockMvc.perform(get("/soul/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteUser() throws Exception{
        mockMvc.perform(delete("/soul/1"))
                .andExpect(status().isNotFound());
    }
}