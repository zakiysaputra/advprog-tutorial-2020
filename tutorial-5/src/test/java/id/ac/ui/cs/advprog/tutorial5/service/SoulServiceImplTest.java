package id.ac.ui.cs.advprog.tutorial5.service;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SoulServiceImplTest {

    @Mock
    private SoulRepository soulRepository;
    private Soul soul;

    @InjectMocks
    private SoulServiceImpl soulService;

    @BeforeEach
    public void setUp(){
        soul = new Soul();
        soul.setAge(17);
        soul.setGender("M");
        soul.setName("Akise Aru");
        soul.setOccupation("Detective");
        soul.setId(1);
    }
    @Test
    public void testFindAll(){
        List<Soul> soulList = soulService.findAll();
        lenient().when(soulService.findAll()).thenReturn(soulList);
    }

    @Test
    public void testRegisterSoul(){
        soulService.register(soul);
        assertNotNull(soulService.findAll());
    }
    @Test
    public void testFindSoul(){
        assertEquals(soul, soulService.register(soul));
    }
    @Test
    public void testErase(){
        soulService.register(soul);
        soulService.erase(soul.getId());
        lenient().when(soulService.findSoul(soul.getId())).thenReturn(Optional.empty());
    }
    @Test
    public void testRewriteSoul(){
        soulService.register(soul);
        Soul soulRewrite = soul;
        soulRewrite.setAge(33);
        assertEquals(soul,soulService.rewrite(soulRewrite));
    }
}

