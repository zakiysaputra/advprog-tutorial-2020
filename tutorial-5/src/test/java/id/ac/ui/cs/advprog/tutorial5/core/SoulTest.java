package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new Soul();
        soul.setAge(21);
        soul.setGender("F");
        soul.setName("Violet Evergarden");
        soul.setOccupation("Auto-Memory Doll");
        soul.setId(1);
    }

    @Test
    void getAndSetGender() {
        assertEquals("F",soul.getGender());
        soul.setGender("X");
        assertEquals("X",soul.getGender());
    }
    @Test
    void getAndSetOccupation() {
        assertEquals("Auto-Memory Doll",soul.getOccupation());
        soul.setOccupation("Messenger");
        assertEquals("Messenger",soul.getOccupation());
    }
    @Test
    void getAndSetName() {
        assertEquals("Violet Evergarden",soul.getName());
        soul.setName("Biolet Ebegaden");
        assertEquals("Biolet Ebegaden",soul.getName());
    }
    @Test
    void getAndSetAge() {
        assertEquals(21,soul.getAge());
        soul.setAge(22);
        assertEquals(22,soul.getAge());
    }
    @Test
    void getAndSetId() {
        assertEquals(1,soul.getId());
        soul.setId(2);
        assertEquals(2,soul.getId());
    }
}
