package id.ac.ui.cs.advprog.tutorial5.controller;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/soul")
public class SoulController {

    @Autowired
    private final SoulService soulService;

    public SoulController(SoulService soulService) {
        this.soulService = soulService;
    }

    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        // DONE: Use service to complete me.
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity create(@RequestBody Soul soul) {
        // DONE: Use service to complete me.
        soulService.register(soul);
        return new ResponseEntity<>(soulService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        // DONE: Use service to complete me.
        Optional<Soul> optionalSoul = soulService.findSoul(id);
        if(!optionalSoul.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Soul>(optionalSoul.get(), HttpStatus.ACCEPTED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        // TODO: Use service to complete me.
        Optional<Soul> optionalSoul = soulService.findSoul(id);
        if(!optionalSoul.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        soul.setId(id);
        return new ResponseEntity<>(soulService.rewrite(soul), HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        Optional<Soul> optionalSoul = soulService.findSoul(id);
        if(!optionalSoul.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        soulService.erase(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
