package id.ac.ui.cs.advprog.tutorial5.repository;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SoulRepository extends JpaRepository<Soul, Long> {

}
