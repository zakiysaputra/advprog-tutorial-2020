package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public class ThousandYearsOfPain implements Skill {
    private String skillName;
    private String skillDescription;

    public ThousandYearsOfPain() {
        this.skillName = "Thousand Years Of Pain";
        this.skillDescription = "Deal a surprise attack";
    }

    @Override
    public String getName() {
        // DONE fix me
        return skillName;
    }

    @Override
    public String getDescription() {
        // DONE fix me
        return skillDescription;
    }
}
