package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public class ShiningForce implements Skill {
    private String skillName;
    private String skillDescription;

    public ShiningForce() {
        this.skillName = "Shining Force";
        this.skillDescription = "Strike your foe with a blinding attack";
    }

    @Override
    public String getName() {
        // DONE fix me
        return skillName;
    }

    @Override
    public String getDescription() {
        // DONE fix me
        return skillDescription;
    }
}
