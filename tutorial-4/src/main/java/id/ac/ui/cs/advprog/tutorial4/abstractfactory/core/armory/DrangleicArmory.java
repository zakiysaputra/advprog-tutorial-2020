package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.MetalArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class DrangleicArmory implements Armory {
    Armor armor = new MetalArmor();
    Weapon weapon = new ThousandJacker();
    Skill skill = new ThousandYearsOfPain();

    @Override
    public Armor craftArmor() {
        // DONE fix me
        return armor;
    }

    @Override
    public Weapon craftWeapon() {
        // DONE fix me
        return weapon;
    }

    @Override
    public Skill learnSkill() {
        // DONE fix me
        return skill;
    }
}
