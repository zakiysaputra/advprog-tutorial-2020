package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon;

public class ThousandJacker implements Weapon {
    private String weaponName;
    private String weaponDescription;

    public ThousandJacker() {
        this.weaponName = "Thousand Jacker";
        this.weaponDescription = "Stab the Opposition";
    }

    @Override
    public String getName() {
        // DONE fix me
        return weaponName;
    }

    @Override
    public String getDescription() {
        // DONE fix me
        return weaponDescription;
    }
}
