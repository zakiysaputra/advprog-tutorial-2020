package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {
    // Metode Eager > Udah dibikin dulu instancenya pas deklarasi
    private String wish;
    private static final HolyWish instance  = new HolyWish();

    // DONE complete me with any Singleton approach
    private HolyWish(){} ;

    public static HolyWish getInstance() {
        return instance;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
