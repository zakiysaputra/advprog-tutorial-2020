package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.DrangleicArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;

public class DrangleicAcademy extends KnightAcademy {

    @Override
    public String getName() {
        // DONE complete me
        return "Drangleic";
    }

    @Override
    protected Knight produceKnight(String type) {
        Knight knight = null;
        Armory armory = new DrangleicArmory();

        switch (type) {
            case "majestic":
                // DONE complete me
                knight = new MajesticKnight(armory);
                knight.setName("Drangleic Majestic Knight");
                break;
            case "metal cluster":
                // DONE complete me
                knight = new MetalClusterKnight(armory);
                knight.setName("Drangleic Metal Cluster Knight");
                break;
            case "synthetic":
                // DONE complete me
                knight = new SyntheticKnight(armory);
                knight.setName("Drangleic Synthetic Knight");
                break;
        }

        return knight;
    }
}
