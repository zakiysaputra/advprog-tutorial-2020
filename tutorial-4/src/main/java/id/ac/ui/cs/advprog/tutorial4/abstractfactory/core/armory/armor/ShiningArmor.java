package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor;

public class ShiningArmor implements Armor {
    private String armorName;
    private String armorDescription;

    public ShiningArmor() {
        this.armorName = "Shining Armor";
        this.armorDescription = "An Armor blessed by The Holy Light.";
    }

    @Override
    public String getName() {
        // DONE fix me
        return armorName;
    }

    @Override
    public String getDescription() {
        // DONE fix me
        return armorDescription;
    }
}
