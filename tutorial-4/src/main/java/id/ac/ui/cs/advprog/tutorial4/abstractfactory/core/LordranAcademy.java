package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.LordranArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

public class LordranAcademy extends KnightAcademy {

    @Override
    public String getName() {
        // DONE complete me
        return "Lordran";
    }

    @Override
    protected Knight produceKnight(String type) {
        Knight knight = null;
        Armory armory = new LordranArmory();

        switch (type) {
            case "majestic":
                // DONE complete me
                knight = new MajesticKnight(armory);
                knight.setName("Lordran Majestic Knight");
                break;
            case "metal cluster":
                // DONE complete me
                knight = new MetalClusterKnight(armory);
                knight.setName("Lordran Metal Cluster Knight");
                break;
            case "synthetic":
                // DONE complete me
                knight = new SyntheticKnight(armory);
                knight.setName("Lordran Synthetic Knight");
                break;
        }

        return knight;
    }
}
