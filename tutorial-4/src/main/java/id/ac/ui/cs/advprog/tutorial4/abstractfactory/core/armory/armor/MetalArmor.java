package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor;

public class MetalArmor implements Armor {
    private String armorName;
    private String armorDescription;

    public MetalArmor() {
        this.armorName = "Metal Armor";
        this.armorDescription = "An Armor crafted with finest Metal.";
    }

    @Override
    public String getName() {
        // DONE fix me
        return armorName;
    }

    @Override
    public String getDescription() {
        // DONE fix me
        return armorDescription;
    }
}
