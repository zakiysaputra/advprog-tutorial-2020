package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.springframework.stereotype.Service;

@Service
public class HolyGrail {
    // DONE complete me
    public HolyGrail() {};
    private HolyWish holyWish = HolyWish.getInstance();

    public void makeAWish(String wish) {
        // DONE complete me
        holyWish.setWish(wish);
    }

    public HolyWish getHolyWish() {
        // DONE fix me
        return holyWish;
    }
}
