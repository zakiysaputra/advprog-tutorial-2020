package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon;

public class ShiningBuster implements Weapon {
    private String weaponName;
    private String weaponDescription;

    public ShiningBuster() {
        this.weaponName = "Shining Buster";
        this.weaponDescription = "Crafted with Pure Light";
    }

    @Override
    public String getName() {
        // DONE fix me
        return weaponName;
    }

    @Override
    public String getDescription() {
        // DONE fix me
        return weaponDescription;
    }
}
