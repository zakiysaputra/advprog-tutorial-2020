package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ShiningBuster;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;

public class LordranArmory implements Armory {
    Armor armor = new ShiningArmor();
    Weapon weapon = new ShiningBuster();
    Skill skill = new ShiningForce();

    @Override
    public Armor craftArmor() {
        // DONE fix me
        return armor;
    }

    @Override
    public Weapon craftWeapon() {
        // DONE fix me
        return weapon;
    }

    @Override
    public Skill learnSkill() {
        // DONE fix me
        return skill;
    }
}
