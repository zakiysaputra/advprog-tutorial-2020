package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // DONE setup me
        drangleicAcademy = new DrangleicAcademy();

        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // DONE create test
        assertTrue(majesticKnight.getArmor() instanceof Armor);
    }

    @Test
    public void checkKnightNames() {
        // DONE create test
        assertEquals("Drangleic Majestic Knight", majesticKnight.getName());
        assertNotEquals("Lordran Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Drangleic Synthetic Knight", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // DONE create test
        assertNull(majesticKnight.getSkill());
        assertNotNull(metalClusterKnight.getSkill());
        assertEquals("Stab the Opposition", syntheticKnight.getWeapon().getDescription());
    }

}
