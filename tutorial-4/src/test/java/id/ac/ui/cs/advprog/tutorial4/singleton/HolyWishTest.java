package id.ac.ui.cs.advprog.tutorial4.singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HolyWishTest {

    private Class<?> holyWishClass;
    private HolyWish holyWish;

    @BeforeEach
    public void setUp() throws Exception {
        holyWishClass = Class.forName(HolyWish.class.getName());
        holyWish = HolyWish.getInstance();
    }

    @Test
    public void testNoPublicConstructors() {
        List<Constructor> constructors = Arrays.asList(holyWishClass.getDeclaredConstructors());

        boolean check = constructors.stream()
                .anyMatch(c -> !Modifier.isPrivate(c.getModifiers()));

        assertFalse(check);
    }

    @Test
    public void testGetInstanceShouldReturnSingletonInstance() {
        // DONE create test
        HolyWish holyWish1 = HolyWish.getInstance();
        assertEquals(holyWish, holyWish1);
        holyWish.setWish("Singleton");
        assertEquals(holyWish.getWish(), holyWish1.getWish());
    }

    @Test
    public void testWish(){
        // DONE create test
        holyWish.setWish(null);
        assertNull(holyWish.getWish());
        holyWish.setWish("testWish");
        assertNotNull(holyWish.getWish());
    }

    @Test
    public void testWishExist(){
        // DONE create test
        holyWish.setWish("Wish Exists!");
        assertEquals("Wish Exists!", holyWish.getWish());
        assertEquals("Wish Exists!", holyWish.toString());
    }

}
