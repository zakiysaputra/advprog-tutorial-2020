package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class LordranAcademyTest {
    KnightAcademy lordranAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // DONE setup me
        lordranAcademy = new LordranAcademy();

        majesticKnight = lordranAcademy.getKnight("majestic");
        metalClusterKnight = lordranAcademy.getKnight("metal cluster");
        syntheticKnight = lordranAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightInstances() {
        // DONE create test
        assertTrue(majesticKnight.getArmor() instanceof Armor);
    }

    @Test
    public void checkKnightNames() {
        // DONE create test
        assertEquals("Lordran Majestic Knight", majesticKnight.getName());
        assertNotEquals("Drangleic Synthetic Knight", syntheticKnight.getName());
        assertEquals("Lordran Metal Cluster Knight", metalClusterKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // DONE create test
        assertNull(syntheticKnight.getArmor());
        assertNotNull(metalClusterKnight.getSkill());
        assertEquals("Crafted with Pure Light", majesticKnight.getWeapon().getDescription());
    }
}
