package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.armor;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.ShiningArmor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiningArmorTest {

    Armor shiningArmor;

    @BeforeEach
    public void setUp(){
        shiningArmor = new ShiningArmor();
    }

    @Test
    public void testToString(){
        // DONE create test
        assertEquals("Shining Armor", shiningArmor.getName());
    }

    @Test
    public void testDescription(){
        // DONE create test
        assertEquals("An Armor blessed by The Holy Light.", shiningArmor.getDescription());
    }
}
