package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // DONE create tests
    public HolyGrail holyGrail;

    @BeforeEach
    public void setUp() throws Exception {
        holyGrail = new HolyGrail();
    }

    @Test
    public void testHolyWishExists() {
        assertNotNull(holyGrail.getHolyWish());
    }

    @Test
    public void testMakeAWish() {
        holyGrail.getHolyWish().setWish("The Holy Wish");
        holyGrail.makeAWish("Wish of The Holy Grail");
        assertEquals("Wish of The Holy Grail", holyGrail.getHolyWish().getWish());
    }
}
