package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.DrangleicArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.LordranArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests
    @BeforeEach
    public void setUp() {
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
    }

    @Test
    public void
    testProduceKnightGetKnight() {
        academyRepository = new AcademyRepository();
        academyService = new AcademyServiceImpl(academyRepository);
        assertNull(academyService.getKnight());
        academyService.produceKnight("Lordran", "majestic");
        assertNotNull(academyService.getKnight());
    }

    @Test
    public void testGetKnightAcademies() {
        assertNotNull(academyService.getKnightAcademies());
    }
}
