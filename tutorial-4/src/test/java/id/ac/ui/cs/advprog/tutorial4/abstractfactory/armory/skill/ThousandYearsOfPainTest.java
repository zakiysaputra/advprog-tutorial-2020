package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.skill;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ThousandYearsOfPain;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ThousandYearsOfPainTest {

    Skill thousandYearsOfPain;

    @BeforeEach
    public void setUp(){
        thousandYearsOfPain = new ThousandYearsOfPain();
    }

    @Test
    public void testToString(){
        // DONE create test
        assertEquals("Thousand Years Of Pain", thousandYearsOfPain.getName());
    }

    @Test
    public void testDescription(){
        // DONE create test
        assertEquals("Deal a surprise attack", thousandYearsOfPain.getDescription());
    }
}
