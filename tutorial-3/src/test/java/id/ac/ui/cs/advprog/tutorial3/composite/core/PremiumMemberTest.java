package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //DONE: Complete me
        assertEquals("Aqua", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //DONE: Complete me
        assertEquals("Goddess", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //DONE: Complete me
        Member dummy = new OrdinaryMember("Darkness", "Lalatina");
        member.addChildMember(dummy);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //DONE: Complete me
        Member dummy = new OrdinaryMember("Megumin", "Explosion");
        member.addChildMember(dummy);
        member.removeChildMember(dummy);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //DONE: Complete me
        Member dummy1 = new OrdinaryMember("Darkness", "Lalatina");
        Member dummy2 = new OrdinaryMember("Megumin", "Explosion");
        Member dummy3 = new OrdinaryMember("Wiz", "Shopkeeper");
        Member dummy4 = new OrdinaryMember("Yunyun", "Thunder");
        member.addChildMember(dummy1);
        member.addChildMember(dummy2);
        member.addChildMember(dummy3);
        member.addChildMember(dummy4);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //DONE: Complete me
        Member kazuma = new PremiumMember("Kazuma", "Master");
        Member dummy1 = new OrdinaryMember("Darkness", "Lalatina");
        Member dummy2 = new OrdinaryMember("Megumin", "Explosion");
        Member dummy3 = new OrdinaryMember("Wiz", "Shopkeeper");
        kazuma.addChildMember(dummy1);
        kazuma.addChildMember(dummy2);
        kazuma.addChildMember(member);
        kazuma.addChildMember(dummy3);
        assertEquals(4, kazuma.getChildMembers().size());
    }
}
