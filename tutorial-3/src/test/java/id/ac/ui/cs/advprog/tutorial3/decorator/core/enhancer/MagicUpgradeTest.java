package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        //DONE: Complete me
        boolean weaponName = magicUpgrade.getName().equals("Longbow");
        assertTrue(weaponName);
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //DONE: Complete me
        boolean weaponDescription = magicUpgrade.getDescription().equals("Magic Big Longbow");
        assertTrue(weaponDescription);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //DONE: Complete me
        int random = magicUpgrade.getWeaponValue();
        int high = 35;
        int low = 30;
        assertTrue(high >= random);
        assertTrue(low  <= random);
    }
}
