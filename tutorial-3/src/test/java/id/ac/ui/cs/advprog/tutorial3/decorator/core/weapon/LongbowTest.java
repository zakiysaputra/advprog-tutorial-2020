package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class LongbowTest {


    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Longbow();
    }

    @Test
    public void testMethodGetWeaponName(){
        //DONE: Complete me
        boolean weaponName = weapon.getName().equals("Longbow");
        assertTrue(weaponName);
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //DONE: Complete me
        boolean weaponDescription = weapon.getDescription().equals("Big Longbow");
        assertTrue(weaponDescription);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //DONE: Complete me
        assertEquals(weapon.getWeaponValue(), 15);
    }
}
