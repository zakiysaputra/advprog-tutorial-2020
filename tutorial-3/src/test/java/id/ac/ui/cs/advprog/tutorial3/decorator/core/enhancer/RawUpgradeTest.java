package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;

    @BeforeEach
    public void setUp(){
        rawUpgrade = new RawUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //DONE: Complete me
        boolean weaponName = rawUpgrade.getName().equals("Shield");
        assertTrue(weaponName);
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //DONE: Complete me
        boolean weaponDescription = rawUpgrade.getDescription().equals("Raw Heater Shield");
        assertTrue(weaponDescription);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //DONE: Complete me
        int random = rawUpgrade.getWeaponValue();
        int high = 20;
        int low = 15;
        assertTrue(high >= random);
        assertTrue(low  <= random);
    }
}
