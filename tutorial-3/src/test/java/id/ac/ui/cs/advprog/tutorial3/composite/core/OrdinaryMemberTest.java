package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Alice", "Waifu");
    }

    @Test
    public void testMethodGetName() {
        //DONE: Complete me
        assertEquals("Alice", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //DONE: Complete me
        assertEquals("Waifu", member.getRole());
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //DONE: Complete me
        Member dummy = new OrdinaryMember("Eugeo", "Husbando");
        member.addChildMember(dummy);
        assertTrue(member.getChildMembers().isEmpty());
        member.removeChildMember(dummy);
        assertTrue(member.getChildMembers().isEmpty());
    }
}
