package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class GunTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Gun();
    }

    @Test
    public void testMethodGetWeaponName(){
        //DONE: Complete me
        boolean weaponName = weapon.getName().equals("Gun");
        assertTrue(weaponName);
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //DONE: Complete me
        boolean weaponDescription = weapon.getDescription().equals("Automatic Gun");
        assertTrue(weaponDescription);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //DONE: Complete me
        assertEquals(weapon.getWeaponValue(), 20);
    }
}
