package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //DONE: Complete me
        Member sakura = new OrdinaryMember("Sakura", "Haruno");
        guild.addMember(guildMaster, sakura);
        assertEquals(1, guildMaster.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveMember() {
        //DONE: Complete me
        Member kakashi = new OrdinaryMember("Kakashi", "Hatake");
        guild.addMember(guildMaster, kakashi);
        guild.removeMember(guildMaster, kakashi);
        assertEquals(0, guildMaster.getChildMembers().size());
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //DONE: Complete me
        Member tsunade = new OrdinaryMember("Tsunade", "Sannin");
        Member orochimaru = new OrdinaryMember("Orochimaru", "Sannin");
        Member fakeTsunade = new OrdinaryMember("Tsunade", "Dummy");
        guild.addMember(guildMaster, tsunade);
        guild.addMember(guildMaster, orochimaru);
        guild.addMember(guildMaster, fakeTsunade);
        Member testTsunade = guild.getMember("Tsunade", "Sannin");
        assertEquals(testTsunade, tsunade);
        Member testOrochimaru = guild.getMember("Orochimaru", "Sannin");
        assertEquals(testOrochimaru, orochimaru);
    }
}
