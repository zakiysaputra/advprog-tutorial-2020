package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SwordTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Sword();
    }

    @Test
    public void testMethodGetWeaponName(){
        //DONE: Complete me
        boolean weaponName = weapon.getName().equals("Sword");
        assertTrue(weaponName);
    }

    @Test
    public void testGetMethodWeaponDescription(){
        //DONE: Complete me
        boolean weaponDescription = weapon.getDescription().equals("Great Sword");
        assertTrue(weaponDescription);
    }

    @Test
    public void testMethodGetWeaponValue(){
        //DONE: Complete me
        assertEquals(weapon.getWeaponValue(), 25);
    }
}
