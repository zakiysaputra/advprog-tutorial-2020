package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
    //DONE: Complete me

    public Shield() {
        this.weaponName = "Shield";
        this.weaponDescription = "Heater Shield";
        this.weaponValue = 10;
    }
}
