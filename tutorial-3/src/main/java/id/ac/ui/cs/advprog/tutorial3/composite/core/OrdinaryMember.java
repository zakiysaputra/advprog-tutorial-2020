package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    //DONE: Complete me
    protected String memberName;
    protected String role;
    protected List<Member> memberList;

    public OrdinaryMember(String memberName, String role) {
        this.memberName = memberName;
        this.role = role;
        this.memberList = new ArrayList<Member>();
    }

    public String getName() {
        return this.memberName;
    }

    public String getRole() {
        return this.role;
    }

    public void addChildMember(Member member) { }
    public void removeChildMember(Member member) { }
    public List<Member> getChildMembers() {
        return memberList;
    }
}
