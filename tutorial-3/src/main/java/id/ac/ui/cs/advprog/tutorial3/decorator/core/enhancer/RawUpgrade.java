package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RawUpgrade extends Weapon {

    Weapon weapon;
    int enhanceBonus;

    public RawUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random ran = new Random();
        this.enhanceBonus = ran.nextInt(5);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 5-10 ++
    @Override
    public int getWeaponValue() {
        //DONE: Complete me
        return weapon.getWeaponValue() + 5 + enhanceBonus;
    }

    @Override
    public String getDescription() {
        //DONE: Complete me
        return "Raw " + weapon.getDescription();
    }
}
