package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;
    int enhanceBonus;

    public RegularUpgrade(Weapon weapon) {

        this.weapon= weapon;
        Random ran = new Random();
        this.enhanceBonus = ran.nextInt(5);
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        //DONE: Complete me
        return weapon.getWeaponValue() + enhanceBonus;
    }

    @Override
    public String getDescription() {
        //DONE: Complete me
        return "Regular " + weapon.getDescription();
    }
}
