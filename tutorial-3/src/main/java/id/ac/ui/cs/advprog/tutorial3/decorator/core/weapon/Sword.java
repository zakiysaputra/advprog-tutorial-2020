package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
    //DONE: Complete me

    public Sword() {
        this.weaponName = "Sword";
        this.weaponDescription = "Great Sword";
        this.weaponValue = 25;
    }
}
