package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //DONE: Complete me
    protected String memberName;
    protected String role;
    protected List<Member> downMemberList;

    public PremiumMember(String memberName, String role) {
        this.memberName = memberName;
        this.role = role;
        this.downMemberList = new ArrayList<Member>();
    }

    public String getName() {
        return this.memberName;
    }

    public String getRole() {
        return this.role;
    }

    public void addChildMember(Member member) {
        if (!(getRole().equals("Master"))) {
            if (getChildMembers().size() < 3) {
                downMemberList.add(member);
            }
        } else {
            downMemberList.add(member);
        }
    }

    public void removeChildMember(Member member) {
        downMemberList.remove(member);
    }

    public List<Member> getChildMembers() {
        return this.downMemberList;
    }
}
