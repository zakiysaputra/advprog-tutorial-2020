package id.ac.ui.cs.tutorial0.service;

public interface AdventurerCalculatorService {
    public String countPowerPotensialFromBirthYear(int birthYear);
}
