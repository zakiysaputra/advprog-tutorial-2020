package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;

    // NEW CHANGES : LAST ELEMENT;
    private Spell latestSpell;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // DONETODO: Complete Me
        Spell goCast = spells.get(spellName);
        goCast.cast();
        this.latestSpell = goCast;
    }

    public void undoSpell() {
        // DONETODO: Complete Me
        if (this.latestSpell != null) {
            this.latestSpell.undo();
            this.latestSpell = null;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
