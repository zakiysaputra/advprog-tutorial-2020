package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // DONETODO: Complete Me
    private ArrayList<Spell> spells = new ArrayList<>();

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast(){
        for (Spell aSpell : spells) {
                aSpell.cast();
        }
    }

    @Override
    public void undo() {
        for (int i = spells.size() -1; i-- > 0; ) {
            spells.get(i).cast();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
