package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
        //ToDo: Complete me

    @Override
    public String defend() {
        return "Bertahan dengan Penghadang(Barrier)!";
    }

    @Override
    public String getType() {
        return "Barrier";
    }
}
